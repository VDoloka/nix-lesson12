package org.nix;

import java.util.Arrays;
import java.util.NoSuchElementException;


public class MyStack<E> implements Stackable<E> {
    private static final int DEFAULT_CAPACITY = 16;
    private final E[] array;
    private int elementsCount;

    @SuppressWarnings("unchecked")
    public MyStack() {
        this.array = (E[]) new Object[DEFAULT_CAPACITY];
    }

    @SuppressWarnings("unchecked")
    public MyStack(int capacity) {
        this.array = (E[]) new Object[capacity];
    }

    @Override
    public boolean isEmpty() {
        return elementsCount == 0;
    }

    @Override
    public boolean isFull() {
        return elementsCount == array.length;
    }

    @Override
    public void add(E element) {
        if (isFull()) {
            throw new ArrayStoreException("Stack is full, fool");
        } else {
            array[elementsCount++] = element;
        }
    }

    @Override
    public E pop() {
        if (elementsCount > 0) {
            E result = array[--elementsCount];
            array[elementsCount] = null;
            return result;
        } else throw new NoSuchElementException();
    }

    @Override
    public E peek() {
        if (elementsCount > 0) {
            return array[elementsCount - 1];
        } else throw new NoSuchElementException();
    }

    @Override
    public String toString() {
        return "MyStack{" +
                "array=" + Arrays.toString(array) +
                '}';
    }
}