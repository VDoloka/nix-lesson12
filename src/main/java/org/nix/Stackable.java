package org.nix;

public interface Stackable<E> {
    boolean isEmpty();

    boolean isFull();

    void add(E element);

    E pop();

    E peek();
}
