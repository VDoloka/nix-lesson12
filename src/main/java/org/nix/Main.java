package org.nix;

public class Main {
    public static void main(String[] args) {
        Stackable<String> stack = new MyStack<>(5);
        System.out.println(stack.isEmpty());
        stack.add("element1");
        stack.add("element2");
        stack.add("element3");

        System.out.println(stack.isEmpty());
        System.out.println(stack);
        System.out.println(stack.peek());
        System.out.println(stack);
        System.out.println(stack.pop());
        System.out.println(stack);
        System.out.println(stack.pop());
        System.out.println(stack.pop());
    }
}