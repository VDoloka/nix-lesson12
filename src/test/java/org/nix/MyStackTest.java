package org.nix;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;
class MyStackTest {

    @Test
    void shouldIsEmptyFalseWhenElementPresent() {
        final int CAPACITY = 10;
        Stackable<Long> stack = new MyStack<>(CAPACITY);
        for (int i = 0; i < CAPACITY; i++) {
            stack.add((long) i);
        }

        boolean result = stack.isEmpty();

        assertThat(result).isFalse();
    }

    @Test
    void shouldIsEmptyTrueWhenNoElements() {
        final int CAPACITY = 10;
        Stackable<Long> stack = new MyStack<>(CAPACITY);
        boolean result = stack.isEmpty();
        assertThat(result).isTrue();
    }


    @Test
    void shouldIsFullTrueWhenElementsEqualsCapacity() {
        final int CAPACITY = 10;
        Stackable<Long> stack = new MyStack<>(CAPACITY);
        for (int i = 0; i < CAPACITY; i++) {
            stack.add((long) i);
        }
        boolean result = stack.isFull();

        assertThat(result).isTrue();
    }

    @Test
    void shouldIsFullFalseWhenElementsLessThenCapacity() {
        final int CAPACITY = 10;
        Stackable<Long> stack = new MyStack<>(CAPACITY);
        for (int i = 0; i < CAPACITY - 1; i++) {
            stack.add((long) i);
        }
        boolean result = stack.isFull();

        assertThat(result).isFalse();
    }


    @Test
    void shouldAddedElementEqualsPeeked() {
        Stackable<Long> stack = new MyStack<>();
        Long value = 1L;

        stack.add(value);
        Long result = stack.peek();

        assertThat(result).isEqualTo(value);
    }

    @Test
    void shouldPopElementDecreaseElementsCount() {
        final int CAPACITY = 10;
        Stackable<Long> stack = new MyStack<>(CAPACITY);
        for (int i = 0; i < CAPACITY ; i++) {
            stack.add((long) i);
        }

        stack.pop();
        boolean isStackFull = stack.isFull();

        assertThat(isStackFull).isFalse();
    }
    @Test
    void shouldPopElementIsEqualsAdded() {
        Stackable<Long> stack = new MyStack<>();
        Long value = 10L;

        stack.add(value);
        Long result = stack.pop();

        assertThat(result).isEqualTo(value);
    }
}